# HW-6 Midterm question brainstorming for the class DSE-i1020 (Intro to Data Science) 

## Multiple choice (total: 8)
1. What is the objective in data structures design?
    * a) make certain operations efficient
    * b) make most operations efficient
    * c) follow the language's design principles
    * d) recreate built-in structures according the designer's principles
1. Which API operations are supported by lists, tuples, dictionaries, sets, and deques?
    * a) size
    * b) contains
    * c) size and contains
    * d) size, contains, add, and remove
1. Which of the following is *not* required for data to be tidy?
    * a) Each variable as a separate column
    * b) Each row as a separate observation
    * c) Each null value dropped
    * d) The applying of a pivot operation to any non-tidy data
1. Pandas does *not* have built-in for convenience which of the following features?
    * a) CSV file importing
    * b) Random number generation
    * c) Pickle file importing
    * d) Datatime parsing
1. When performing topic analysis on several corpora, what step comes before collecting word frequency?
    * a) A t-space tranformation on dataset
    * b) Stopword removal
    * c) The combining of all corpora into one group
    * d) Fit a LatentDirichletAllocation object
    * e) Both b) and d)
1. BeautifySoup provides a wealth of features, but unfortunately it lacks the ability to _blank_?
    * a) Navigate from one sibiling html element to the next
    * b) Prettify a html document
    * c) Delete html elements
    * d) Modify the string attribute of html element in place
1. Which of the following statements is true about Folium?
    * a) It lets you take the dataset you've manipulated in Python and create interactive maps through web technology
    * b) It is the leading open-source JavaScript library for mobile-friendly interactive maps
    * c) It provides tight integration with Google Maps tilesets
    * d) It accepts the geo data format know as shapefiles, but not the GeoJSON format
    * e) both a) and d)
    * f) both b) and d)
1. Which algorithm is not like the others?
    * a) Principal Component Analysis
    * b) Logistic regression
    * c) Non-negative matrix factorization
    * d) K-means

## True-false choice (total: 7)
1. Python's deque data structure is perfect for FIFO operations but not LIFO ones.
1. The difference between max() and argmax() in Numpy is that latter can be used on multi-dimensional arrays.
1. Word stemming helps collapse words like running, ran, and runs into one word--run.
1. Compared to the requests library, urllib offers a simplier, more friendly API, but both can be used to accomplish the same fundamental task.
1. GeoJSON and shapefiles are often used together for plotting geo data.
1. Unlike, raw matplotlib, seaborn offers built-in support for pandas DataFrames.
1. Classification is a special case of regression where the labels are continuous values.

## Short-answer Data Camp -styled Notebook questions (total: 5)
1. [Question](/short-answer-1.ipynb) - [solution](/short-answer-1-solution.ipynb)
1. [Question](/short-answer-2.ipynb) - [solution](/short-answer-2-solution.ipynb)
1.
1.
1.

## Mini task on dataset (total: 1)


## Answers

### Multiple choice
1. a
1. c
1. c
1. b
1. b
1. d
1. a
1. b

### True-false choice
1. False
1. False
1. True
1. False
1. False
1. False
1. False

### Short-answer

### Mini task

## Concepts covered (also called sources)

1. Standard Library
    - collections
    - csv
    - datetime
    - math
    - random
    - re
    - Json
1. Scipy basic stack:
    - numpy
    - scipy
    - matplotlib
    - pandas
1. NLP:
    - nltk
    - gensim
    - spacy
    - polyglot
1. APIs and Scraping:
    - requests
    - BeautifulSoup
1. Geospatial libraries:
    - geopandas
    - shapely
    - folium
    - xarray
1. Visualization:
    - seaborn
1. Scikit:
    - sklearn
    - scikit-image
